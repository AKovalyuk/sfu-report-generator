import { Component, createEffect, onCleanup } from 'solid-js';
import { EditorPage } from './pages/editor';

export const Page: Component = () => {
  let ref: HTMLDivElement;

  const setHeight = () => {
    ref.style.height = `${window.innerHeight}px`;
  };

  createEffect(() => {
    setHeight();
    window.addEventListener('resize', setHeight);
  });

  onCleanup(() => window.removeEventListener('resize', setHeight));

  return (
    <div ref={ref} class="p-6 bg-gray-300">
      <EditorPage />
    </div>
  );
};
