import type { Component } from 'solid-js';
import type { Report } from './types/report';
import { ReportProvider } from './contexts/report';
import { Page } from './page';
import { SegmentType } from './types/segments';
import { SelectedSegmentProvider } from './contexts/selected';
import { Paragraph } from '@/types/elements';

export const App: Component = () => {
  const report: Report = {
    chapters: [
      {
        type: SegmentType.Chapter,
        title: 'Раздел А',
        elements: [],
        subchapters: [
          {
            type: SegmentType.Subchapter,
            title: 'Подраздел В',
            elements: [],
            sections: [
              {
                type: SegmentType.Section,
                title: 'Пункт Г',
                elements: [],
                subsections: [
                  {
                    type: SegmentType.Subsection,
                    title: 'Подпункт Д',
                    elements: [],
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        type: SegmentType.Chapter,
        title: 'Раздел Б',
        elements: [new Paragraph('some content')],
        subchapters: [],
      },
    ],
  };

  return (
    <ReportProvider report={report}>
      <SelectedSegmentProvider>
        <Page />
      </SelectedSegmentProvider>
    </ReportProvider>
  );
};
