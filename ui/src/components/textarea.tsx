import { Component, createEffect, createSignal, onMount, JSX } from 'solid-js';

export const TextArea: Component<
  JSX.TextareaHTMLAttributes<HTMLTextAreaElement> & {
    ontype: (value: any) => void;
  }
> = (props) => {
  const [value, setValue] = createSignal(props.value);
  createEffect(() => {
    props.ontype(value());
  });

  let ref: HTMLTextAreaElement;
  onMount(() => {
    if (props.autofocus) ref.focus();
  });

  return (
    <textarea
      {...props}
      ref={ref}
      oninput={() => setValue(ref.value)}
      class={`w-full p-2 bg-gray-200 rounded-lg border-none focus:focus:ring-2 focus:ring-gray-500 ${
        props.class || ''
      }`}
    />
  );
};
