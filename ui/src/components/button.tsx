import { Component, onMount } from 'solid-js';
import type { JSX } from 'solid-js';

export const Button: Component<JSX.ButtonHTMLAttributes<HTMLButtonElement>> = (
  props,
) => {
  let ref: HTMLButtonElement;
  onMount(() => {
    if (props.autofocus) ref.focus();
  });

  return (
    <button
      {...props}
      ref={ref}
      class={`p-2 text-sm font-medium uppercase bg-gray-200 rounded-lg hover:ring-2 hover:ring-gray-500 focus:ring-gray-500 ${
        props.class || ''
      }`}
    />
  );
};
