export enum SegmentElementType {
  Paragraph = 'Paragraph',
}

export class SegmentElement {
  type: SegmentElementType;

  constructor(type: SegmentElementType) {
    this.type = type;
  }
}

export class Paragraph extends SegmentElement {
  contents: string;

  constructor(contents: string) {
    super(SegmentElementType.Paragraph);
    this.contents = contents;
  }
}
