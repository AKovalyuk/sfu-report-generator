import type { Chapter } from './segments';

export type Report = {
  chapters: Chapter[];
};
