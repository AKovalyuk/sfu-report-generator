import type { SegmentElement } from './elements';

export type Segment = Chapter | Subchapter | Section | Subsection;

export type SegmentWithPath =
  | ChapterWithPath
  | SubchapterWithPath
  | SectionWithPath
  | SubsectionWithPath;

export type SelectedSegment =
  | SelectedChapter
  | SelectedSubchapter
  | SelectedSection
  | SelectedSubsection;

export enum SegmentType {
  Chapter = 'Chapter',
  Subchapter = 'Subchapter',
  Section = 'Section',
  Subsection = 'Subsection',
}

type GenericSegment = {
  title: string;
  elements: SegmentElement[];
};

export type Chapter = GenericSegment & {
  type: SegmentType.Chapter;
  subchapters: Subchapter[];
};

export type ChapterWithPath = Chapter & {
  type: SegmentType.Chapter;
  path: ChapterPath;
};

export type SelectedChapter = {
  type: SegmentType.Chapter;
  path: ChapterPath;
};

export type Subchapter = GenericSegment & {
  type: SegmentType.Subchapter;
  sections: Section[];
};

export type SubchapterWithPath = Subchapter & {
  type: SegmentType.Subchapter;
  path: SubchapterPath;
};

export type SelectedSubchapter = {
  type: SegmentType.Subchapter;
  path: SubchapterPath;
};

export type Section = GenericSegment & {
  type: SegmentType.Section;
  subsections: Subsection[];
};

export type SectionWithPath = Section & {
  type: SegmentType.Section;
  path: SectionPath;
};

export type SelectedSection = {
  type: SegmentType.Section;
  path: SectionPath;
};

export type Subsection = GenericSegment & {
  type: SegmentType.Subsection;
};

export type SubsectionWithPath = GenericSegment & {
  type: SegmentType.Subsection;
  path: SubsectionPath;
};

export type SelectedSubsection = {
  type: SegmentType.Subsection;
  path: SubsectionPath;
};

export type ChapterPath = { chapter: number };
export type SubchapterPath = ChapterPath & { subchapter: number };
export type SectionPath = SubchapterPath & { section: number };
export type SubsectionPath = SectionPath & { subsection: number };

export const chapterPathsEqual = (p1: ChapterPath, p2: ChapterPath) =>
  p1.chapter == p2.chapter;

export const subchapterPathsEqual = (p1: SubchapterPath, p2: SubchapterPath) =>
  p1.chapter == p2.chapter && p1.subchapter == p2.subchapter;

export const sectionPathsEqual = (p1: SectionPath, p2: SectionPath) =>
  p1.chapter == p2.chapter &&
  p1.subchapter == p2.subchapter &&
  p1.section == p2.section;

export const subsectionPathsEqual = (p1: SubsectionPath, p2: SubsectionPath) =>
  p1.chapter == p2.chapter &&
  p1.subchapter == p2.subchapter &&
  p1.section == p2.section &&
  p1.subsection == p2.subsection;

export const hierarchyTitle = (seg: SegmentWithPath) => {
  switch (seg.type) {
    case SegmentType.Chapter: {
      const { title, path } = seg as ChapterWithPath;
      return `${path.chapter + 1}. ${title}`;
    }
    case SegmentType.Subchapter: {
      const { title, path } = seg as SubchapterWithPath;
      return `${path.chapter + 1}.${path.subchapter + 1}. ${title}`;
    }
    case SegmentType.Section: {
      const { title, path } = seg as SectionWithPath;
      return (
        `${path.chapter + 1}.` +
        `${path.subchapter + 1}.` +
        `${path.section + 1}. ` +
        `${title}`
      );
    }
    case SegmentType.Subsection: {
      const { title, path } = seg as SubsectionWithPath;
      return (
        `${path.chapter + 1}.` +
        `${path.subchapter + 1}.` +
        `${path.section + 1}.` +
        `${path.subsection + 1}. ` +
        `${title}`
      );
    }
  }
};
