import { Component, createContext, createSignal } from 'solid-js';
import {
  ChapterPath,
  SectionPath,
  chapterPathsEqual,
  sectionPathsEqual,
  SegmentType,
  SelectedChapter,
  SelectedSection,
  SelectedSegment,
  SelectedSubchapter,
  SelectedSubsection,
  SubchapterPath,
  subchapterPathsEqual,
  SubsectionPath,
  subsectionPathsEqual,
} from '@/types/segments';

type IsSegmentFunc<P> = (path: P) => boolean;
type SelectSegmentFunc<P> = (path: P) => void;

type SelectedSegmentStore = [
  () => SelectedSegment,
  {
    isChapter: IsSegmentFunc<ChapterPath>;
    isSubchapter: IsSegmentFunc<SubchapterPath>;
    isSection: IsSegmentFunc<SectionPath>;
    isSubsection: IsSegmentFunc<SubsectionPath>;
    selectChapter: SelectSegmentFunc<ChapterPath>;
    selectSubchapter: SelectSegmentFunc<SubchapterPath>;
    selectSection: SelectSegmentFunc<SectionPath>;
    selectSubsection: SelectSegmentFunc<SubsectionPath>;
    unselect: () => void;
  },
];

export const SelectedSegmentContext = createContext<SelectedSegmentStore>();

export const SelectedSegmentProvider: Component = (props) => {
  const [selected, setSelected] = createSignal<SelectedSegment | null>(null);

  const store: SelectedSegmentStore = [
    selected,
    {
      isChapter: (path) => {
        if (!selected() || selected().type != SegmentType.Chapter) {
          return false;
        }
        return chapterPathsEqual((selected() as SelectedChapter).path, path);
      },
      isSubchapter: (path) => {
        if (!selected() || selected().type != SegmentType.Subchapter) {
          return false;
        }
        return subchapterPathsEqual(
          (selected() as SelectedSubchapter).path,
          path,
        );
      },
      isSection: (path) => {
        if (!selected() || selected().type != SegmentType.Section) {
          return false;
        }
        return sectionPathsEqual((selected() as SelectedSection).path, path);
      },
      isSubsection: (path) => {
        if (!selected() || selected().type != SegmentType.Subsection) {
          return false;
        }
        return subsectionPathsEqual(
          (selected() as SelectedSubsection).path,
          path,
        );
      },
      selectChapter: (path) => {
        console.log('[SelectedContext] Selecting chapter', path);
        setSelected({ type: SegmentType.Chapter, path });
      },
      selectSubchapter: (path) => {
        console.log('[SelectedContext] Selecting subchapter', path);
        setSelected({ type: SegmentType.Subchapter, path });
      },
      selectSection: (path) => {
        console.log('[SelectedContext] Selecting section', path);
        setSelected({ type: SegmentType.Section, path });
      },
      selectSubsection: (path) => {
        console.log('[SelectedContext] Selecting subsection', path);
        setSelected({ type: SegmentType.Subsection, path });
      },
      unselect: () => {
        console.log('[SelectedContext] Unselecting');
        setSelected(null);
      },
    },
  ];

  return (
    <SelectedSegmentContext.Provider value={store}>
      {props.children}
    </SelectedSegmentContext.Provider>
  );
};
