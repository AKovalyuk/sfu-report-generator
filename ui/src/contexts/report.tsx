import { Component, createContext, createState, State } from 'solid-js';
import type {
  Chapter,
  ChapterPath,
  ChapterWithPath,
  Section,
  SectionPath,
  SectionWithPath,
  Subchapter,
  SubchapterPath,
  SubchapterWithPath,
  Subsection,
  SubsectionPath,
  SubsectionWithPath,
} from '@/types/segments';
import type { Report } from '@/types/report';

type GetSegmentFunc<S, P> = (path: P) => S;
type AddSegmentFunc<S, P> = (segment: S, parentPath: P) => void;
type UpdSegmentFunc<S, P> = (segment: S, path: P) => void;
type DelSegmentFunc<P> = (path: P) => void;

type SegmentStorage<Segment, SegmentWithPath, Path, ParentPath> = {
  get: GetSegmentFunc<SegmentWithPath, Path>;
  add: AddSegmentFunc<Segment, ParentPath>;
  update: UpdSegmentFunc<Segment, Path>;
  delete: DelSegmentFunc<Path>;
};

type ReportStore = [
  State<Report>,
  {
    chapters: SegmentStorage<Chapter, ChapterWithPath, ChapterPath, null>;
    subchapters: SegmentStorage<
      Subchapter,
      SubchapterWithPath,
      SubchapterPath,
      ChapterPath
    >;
    sections: SegmentStorage<
      Section,
      SectionWithPath,
      SectionPath,
      SubchapterPath
    >;
    subsections: SegmentStorage<
      Subsection,
      SubsectionWithPath,
      SubsectionPath,
      SectionPath
    >;
  },
];

export const ReportContext = createContext<ReportStore>();

export const ReportProvider: Component<{ report: Report }> = (props) => {
  const [report, setReport] = createState(props.report);
  const store: ReportStore = [
    report,
    {
      chapters: {
        get: (path) => {
          console.log('[ReportContext] Getting chapter', path);
          return { ...report.chapters[path.chapter], path };
        },
        add: (chapter) => {
          console.log('[ReportContext] Adding chapter', chapter);
          setReport('chapters', (chapters) => [...chapters, chapter]);
        },
        update: (chapter, path) => {
          console.log('[ReportContext] Updating chapter', chapter, 'in', path);
          setReport('chapters', path.chapter, () => chapter);
        },
        delete: (path) => {
          console.log('[ReportContext] Deleting chapter', path);
          setReport('chapters', (chapters) =>
            chapters.filter((_, i) => i != path.chapter),
          );
        },
      },
      subchapters: {
        get: (path) => {
          console.log('[ReportContext] Getting subchapter', path);
          return {
            ...report.chapters[path.chapter].subchapters[path.subchapter],
            path,
          };
        },
        add: (subchapter, path) => {
          console.log('[ReportContext] Adding subchapter', subchapter);
          setReport('chapters', path.chapter, 'subchapters', (subchapters) => [
            ...subchapters,
            subchapter,
          ]);
        },
        update: (subchapter, path) => {
          console.log(
            '[ReportContext] Updating subchapter',
            subchapter,
            'in',
            path,
          );
          setReport(
            'chapters',
            path.chapter,
            'subchapters',
            path.subchapter,
            () => subchapter,
          );
        },
        delete: (path) => {
          console.log('[ReportContext] Deleting subchapter', path);
          setReport('chapters', path.chapter, 'subchapters', (subchapters) =>
            subchapters.filter((_, i) => i != path.subchapter),
          );
        },
      },
      sections: {
        get: (path) => {
          console.log('[ReportContext] Getting section', path);
          return {
            // prettier-ignore
            ...report
              .chapters[path.chapter]
              .subchapters[path.subchapter]
              .sections[path.section],
            path,
          };
        },
        add: (section, path) => {
          console.log('[ReportContext] Adding section', section);
          setReport(
            'chapters',
            path.chapter,
            'subchapters',
            path.subchapter,
            'sections',
            (sections) => [...sections, section],
          );
        },
        update: (section, path) => {
          console.log('[ReportContext] Updating section', section, 'in', path);
          setReport(
            'chapters',
            path.chapter,
            'subchapters',
            path.subchapter,
            'sections',
            path.section,
            () => section,
          );
        },
        delete: (path) => {
          console.log('[ReportContext] Deleting section', path);
          setReport(
            'chapters',
            path.chapter,
            'subchapters',
            path.subchapter,
            'sections',
            (sections) => sections.filter((_, i) => i != path.section),
          );
        },
      },
      subsections: {
        get: (path) => {
          console.log('[ReportContext] Getting subsection', path);
          return {
            // prettier-ignore
            ...report
              .chapters[path.chapter]
              .subchapters[path.subchapter]
              .sections[path.section]
              .subsections[path.subsection],
            path,
          };
        },
        add: (subsection, path) => {
          console.log(
            '[ReportContext] Adding subsection',
            subsection,
            'to',
            path,
          );
          setReport(
            'chapters',
            path.chapter,
            'subchapters',
            path.subchapter,
            'sections',
            path.section,
            'subsections',
            (subsections) => [...subsections, subsection],
          );
        },
        update: (subsection, path) => {
          console.log(
            '[ReportContext] Updating subsection',
            subsection,
            'in',
            path,
          );
          setReport(
            'chapters',
            path.chapter,
            'subchapters',
            path.subchapter,
            'sections',
            path.section,
            'subsections',
            path.subsection,
            () => subsection,
          );
        },
        delete: (path) => {
          console.log('[ReportContext] Deleting subsection', path);
          setReport(
            'chapters',
            path.chapter,
            'subchapters',
            path.subchapter,
            'sections',
            path.section,
            'subsections',
            (subsections) => subsections.filter((_, i) => i != path.subsection),
          );
        },
      },
    },
  ];

  return (
    <ReportContext.Provider value={store}>
      {props.children}
    </ReportContext.Provider>
  );
};
