import { Component, createSignal, Match, Show, Switch } from 'solid-js';
import { Card } from '@/components/card';
import { PenIcon } from '@/components/icons/pen';
import { TrashIcon } from '@/components/icons/trash';
import * as elements from '@/types/elements';
import { TextAreaModal } from '@/pages/editor/segment/modals';

export const SegmentElement: Component<{
  element: elements.SegmentElement;
  onUpdate: (el: elements.SegmentElement) => void;
  onDelete: () => void;
}> = (props) => {
  return (
    <Switch>
      <Match when={props.element.type == elements.SegmentElementType.Paragraph}>
        <ParagraphElement
          paragraph={props.element as elements.Paragraph}
          onUpdate={(text) => props.onUpdate(new elements.Paragraph(text))}
          onDelete={props.onDelete}
        />
      </Match>
    </Switch>
  );
};

const ParagraphElement: Component<{
  paragraph: elements.Paragraph;
  onUpdate: (contents: string) => void;
  onDelete: () => void;
}> = (props) => {
  const [showEditModal, setShowEditModal] = createSignal(false);

  return (
    <>
      <Show when={showEditModal()}>
        <TextAreaModal
          prevText={props.paragraph.contents}
          submit={props.onUpdate}
          close={() => setShowEditModal(false)}
        />
      </Show>
      <ElementCard
        label="Абзац"
        onClickEdit={() => setShowEditModal(true)}
        onClickDelete={props.onDelete}
      >
        <p class="leading-snug overflow-ellipsis overflow-hidden">
          {props.paragraph.contents}
        </p>
      </ElementCard>
    </>
  );
};

const ElementCard: Component<{
  label: string;
  onClickEdit: () => void;
  onClickDelete: () => void;
}> = (props) => {
  return (
    <Card class="mt-6">
      <div class="flex justify-between items-center">
        <h6 class="text-sm font-bold uppercase">{props.label}</h6>
        <div class="flex space-x-2">
          <button onclick={props.onClickEdit}>
            <PenIcon class="w-5 h-5" />
          </button>
          <button onclick={props.onClickDelete}>
            <TrashIcon class="w-5 h-5" />
          </button>
        </div>
      </div>
      <div class="mt-1">{props.children}</div>
    </Card>
  );
};
