import { Modal } from '@/components/modal';
import { Input } from '@/components/input';
import { Button } from '@/components/button';
import { TextArea } from '@/components/textarea';
import { Component, createSignal, Show } from 'solid-js';
import { Paragraph, SegmentElement } from '@/types/elements';

export const ChangeTitleModal: Component<{
  prevTitle: string;
  update: (title: string) => void;
  close: () => void;
}> = (props) => {
  const [title, setTitle] = createSignal('');

  return (
    <Modal close={props.close}>
      <form
        class="flex flex-col items-end"
        onsubmit={(e) => {
          e.preventDefault();
          props.update(title());
          props.close();
        }}
      >
        <Input
          type="text"
          value={props.prevTitle}
          placeholder="Введите новое название"
          ontype={(title) => setTitle(title as string)}
          autofocus
        />
        <div class="mt-4">
          <Button type="submit">Сохранить</Button>
        </div>
      </form>
    </Modal>
  );
};

export const ConfirmModal: Component<{
  message: string;
  confirm: () => void;
  close: () => void;
}> = (props) => {
  return (
    <Modal close={props.close}>
      <p class="mt-2 text-center leading-snug">{props.message}</p>
      <div class="mt-4 flex justify-center space-x-2">
        <Button
          type="button"
          onclick={props.close}
          autofocus
          tabindex="1"
          class="w-20"
        >
          Отмена
        </Button>
        <Button type="button" onclick={props.confirm} tabindex="2" class="w-20">
          Ок
        </Button>
      </div>
    </Modal>
  );
};

export const TextAreaModal: Component<{
  prevText?: string;
  submit: (text: string) => void;
  close: () => void;
}> = (props) => {
  const [text, setText] = createSignal(props.prevText || '');

  return (
    <Modal close={props.close}>
      <TextArea
        value={text()}
        ontype={(text) => setText(text)}
        rows="10"
        tabindex="1"
        autofocus
      />
      <div class="mt-4 flex justify-end">
        <Button
          type="button"
          onclick={() => {
            props.submit(text());
            props.close();
          }}
          tabindex="2"
          class="w-20"
        >
          Ок
        </Button>
      </div>
    </Modal>
  );
};
export const AddChapterContentModal: Component<{
  onAddElement: (element: SegmentElement) => void;
  onAddSubchaper: () => void;
  showAddElement: boolean;
  showAddSubchapter: boolean;
  close: () => void;
}> = (props) => {
  return (
    <AddSegmentContentModal
      close={props.close}
      onAddElement={props.onAddElement}
      onAddChildSegment={props.onAddSubchaper}
      showAddElement={props.showAddElement}
      showAddChildSegment={props.showAddSubchapter}
      childSegmentTitle="Подраздел"
    />
  );
};

export const AddSubchapterContentModal: Component<{
  onAddElement: (element: SegmentElement) => void;
  onAddSection: () => void;
  showAddElement: boolean;
  showAddSection: boolean;
  close: () => void;
}> = (props) => {
  return (
    <AddSegmentContentModal
      close={props.close}
      onAddElement={props.onAddElement}
      onAddChildSegment={props.onAddSection}
      showAddElement={props.showAddElement}
      showAddChildSegment={props.showAddSection}
      childSegmentTitle="Пункт"
    />
  );
};

export const AddSectionContentModal: Component<{
  onAddElement: (element: SegmentElement) => void;
  onAddSubsection: () => void;
  showAddElement: boolean;
  showAddSubsection: boolean;
  close: () => void;
}> = (props) => {
  return (
    <AddSegmentContentModal
      close={props.close}
      onAddElement={props.onAddElement}
      onAddChildSegment={props.onAddSubsection}
      showAddElement={props.showAddElement}
      showAddChildSegment={props.showAddSubsection}
      childSegmentTitle="Подпункт"
    />
  );
};

export const AddSubsectionContentModal: Component<{
  onAddElement: (element: SegmentElement) => void;
  close: () => void;
}> = (props) => {
  return (
    <AddSegmentContentModal
      close={props.close}
      onAddElement={props.onAddElement}
      onAddChildSegment={() => {}}
      showAddElement={true}
      showAddChildSegment={false}
      childSegmentTitle=""
    />
  );
};

const AddSegmentContentModal: Component<{
  close: () => void;
  onAddElement: (element: SegmentElement) => void;
  onAddChildSegment: () => void;
  showAddElement: boolean;
  showAddChildSegment: boolean;
  childSegmentTitle: string;
}> = (props) => {
  return (
    <Modal close={props.close}>
      <div class="flex flex-col space-y-2">
        <Show when={props.showAddChildSegment}>
          <AddChildSegmentButton
            segmentTitle={props.childSegmentTitle}
            onclick={() => {
              props.onAddChildSegment();
              props.close();
            }}
          />
        </Show>
        <Show when={props.showAddElement}>
          <AddParagraphButton
            onclick={(p) => {
              props.onAddElement(p);
              props.close();
            }}
          />
        </Show>
      </div>
    </Modal>
  );
};

const AddChildSegmentButton: Component<{
  onclick: () => void;
  segmentTitle: string;
}> = (props) => {
  return <Button onclick={props.onclick}>{props.segmentTitle}</Button>;
};

const AddParagraphButton: Component<{
  onclick: (paragraph: Paragraph) => void;
}> = (props) => {
  const [showModal, setShowModal] = createSignal(false);

  return (
    <>
      <Button onclick={() => setShowModal(true)}>Абзац</Button>
      <Show when={showModal()}>
        <TextAreaModal
          close={() => setShowModal(false)}
          submit={(text) => {
            props.onclick(new Paragraph(text));
            setShowModal(false);
          }}
        />
      </Show>
    </>
  );
};
