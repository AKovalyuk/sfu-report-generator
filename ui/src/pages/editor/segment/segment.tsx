import {
  Component,
  createEffect,
  createMemo,
  createSignal,
  createState,
  For,
  Match,
  Show,
  Switch,
  useContext,
} from 'solid-js';
import * as segments from '@/types/segments';
import type * as elements from '@/types/elements';
import { ReportContext } from '@/contexts/report';
import { SelectedSegmentContext } from '@/contexts/selected';
import { Button, Card } from '@/components';
import { ArrowRight, PenIcon, PlusIcon, TrashIcon } from '@/components/icons';
import {
  AddSubsectionContentModal,
  AddSectionContentModal,
  AddSubchapterContentModal,
  AddChapterContentModal,
  ChangeTitleModal,
  ConfirmModal,
} from '@/pages/editor/segment/modals';
import { SegmentElement } from './elements';

export const SegmentEditor: Component = () => {
  const [selected] = useContext(SelectedSegmentContext);
  const [_, { chapters, subchapters, sections, subsections }] = useContext(
    ReportContext,
  );
  const segment = createMemo(() => {
    switch (selected() && selected().type) {
      case segments.SegmentType.Chapter:
        return chapters.get((selected() as segments.SelectedChapter).path);
      case segments.SegmentType.Subchapter:
        return subchapters.get(
          (selected() as segments.SelectedSubchapter).path,
        );
      case segments.SegmentType.Section:
        return sections.get((selected() as segments.SelectedSection).path);
      case segments.SegmentType.Subsection:
        return subsections.get(
          (selected() as segments.SelectedSubsection).path,
        );
    }
  });

  return (
    <Show when={selected()}>
      <Switch>
        <Match when={segment().type == segments.SegmentType.Chapter}>
          <ChapterEditor chapter={segment() as segments.ChapterWithPath} />
        </Match>
        <Match when={segment().type == segments.SegmentType.Subchapter}>
          <SubchapterEditor
            subchapter={segment() as segments.SubchapterWithPath}
          />
        </Match>
        <Match when={segment().type == segments.SegmentType.Section}>
          <SectionEditor section={segment() as segments.SectionWithPath} />
        </Match>
        <Match when={segment().type == segments.SegmentType.Subsection}>
          <SubsectionEditor
            subsection={segment() as segments.SubsectionWithPath}
          />
        </Match>
      </Switch>
    </Show>
  );
};

const ChapterEditor: Component<{ chapter: segments.ChapterWithPath }> = (
  props,
) => {
  const [_s, { unselect, selectChapter, selectSubchapter }] = useContext(
    SelectedSegmentContext,
  );
  const [report, { chapters }] = useContext(ReportContext);
  const [handlers, setHandlers] = createState<{
    delete: () => void;
    updateTitle: (title: string) => void;
    addElement: (el: elements.SegmentElement) => void;
    updateElement: (pos: number) => (el: elements.SegmentElement) => void;
    deleteElement: (pos: number) => () => void;
    addSubchapter: () => void;
  }>(null);
  const [showAddContentModal, setShowAddContentModal] = createSignal(false);

  const setupHandlers = () => {
    setHandlers({
      delete: () => {
        const path = props.chapter.path; // need to copy it before select.
        if (report.chapters.length == 1) {
          unselect();
        } else if (path.chapter == report.chapters.length - 1) {
          selectChapter({ chapter: path.chapter - 1 });
        }
        chapters.delete(path);
      },
      updateTitle: (title) => {
        chapters.update({ ...props.chapter, title }, props.chapter.path);
      },
      addElement: (element: elements.SegmentElement) => {
        chapters.update(
          {
            ...props.chapter,
            elements: [...props.chapter.elements, element],
          },
          props.chapter.path,
        );
      },
      updateElement: (pos: number) => (element: elements.SegmentElement) => {
        chapters.update(
          {
            ...props.chapter,
            elements: props.chapter.elements.map((el, i) =>
              i == pos ? element : el,
            ),
          },
          props.chapter.path,
        );
      },
      deleteElement: (pos: number) => () => {
        chapters.update(
          {
            ...props.chapter,
            elements: props.chapter.elements.filter((_, i) => i != pos),
          },
          props.chapter.path,
        );
      },
      addSubchapter: () => {
        chapters.update(
          {
            ...props.chapter,
            subchapters: [
              ...props.chapter.subchapters,
              {
                type: segments.SegmentType.Subchapter,
                title: 'Бызымянный подраздел',
                elements: [],
                sections: [],
              },
            ],
          },
          props.chapter.path,
        );
      },
    });
  };

  createEffect(() => setupHandlers(), setupHandlers());

  return (
    <>
      <SegmentHeading
        title={props.chapter.title}
        hierarchyTitle={segments.hierarchyTitle(props.chapter)}
        onDelete={handlers.delete}
        onTitleUpdate={handlers.updateTitle}
      />
      <For each={props.chapter.elements}>
        {(el, i) => (
          <SegmentElement
            element={el}
            onUpdate={handlers.updateElement(i())}
            onDelete={handlers.deleteElement(i())}
          />
        )}
      </For>
      <For each={props.chapter.subchapters}>
        {(subchapter, i) => (
          <ChildSegmentLink
            onclick={() =>
              selectSubchapter({
                ...props.chapter.path,
                subchapter: i(),
              })
            }
            hierarchyTitle={segments.hierarchyTitle({
              ...subchapter,
              path: { ...props.chapter.path, subchapter: i() },
            })}
          />
        )}
      </For>
      <AddContentButton onclick={() => setShowAddContentModal(true)} />
      <Show when={showAddContentModal()}>
        <AddChapterContentModal
          close={() => setShowAddContentModal(false)}
          onAddElement={(el) => handlers.addElement(el)}
          onAddSubchaper={() => handlers.addSubchapter()}
          showAddElement={props.chapter.subchapters.length == 0}
          showAddSubchapter={props.chapter.elements.length == 0}
        />
      </Show>
    </>
  );
};

const SubchapterEditor: Component<{
  subchapter: segments.SubchapterWithPath;
}> = (props) => {
  const [_s, { selectChapter, selectSubchapter, selectSection }] = useContext(
    SelectedSegmentContext,
  );
  const [_r, { chapters, subchapters }] = useContext(ReportContext);
  const [handlers, setHandlers] = createState<{
    delete: () => void;
    updateTitle: (title: string) => void;
    addElement: (el: elements.SegmentElement) => void;
    updateElement: (pos: number) => (el: elements.SegmentElement) => void;
    deleteElement: (pos: number) => () => void;
    addSection: () => void;
  }>(null);
  const [showAddContentModal, setShowAddContentModal] = createSignal(false);

  const setupHandlers = () => {
    setHandlers({
      delete: () => {
        const path = props.subchapter.path; // need to copy it before select.
        const chapter = chapters.get(path);
        if (chapter.subchapters.length == 1) {
          selectChapter({ chapter: path.chapter });
        } else if (path.subchapter == chapter.subchapters.length - 1) {
          selectSubchapter({ ...path, subchapter: path.subchapter - 1 });
        }
        subchapters.delete(path);
      },
      updateTitle: (title) => {
        subchapters.update(
          { ...props.subchapter, title },
          props.subchapter.path,
        );
      },
      addElement: (element: elements.SegmentElement) => {
        subchapters.update(
          {
            ...props.subchapter,
            elements: [...props.subchapter.elements, element],
          },
          props.subchapter.path,
        );
      },
      updateElement: (pos: number) => (element: elements.SegmentElement) => {
        subchapters.update(
          {
            ...props.subchapter,
            elements: props.subchapter.elements.map((el, i) =>
              i == pos ? element : el,
            ),
          },
          props.subchapter.path,
        );
      },
      deleteElement: (pos: number) => () => {
        subchapters.update(
          {
            ...props.subchapter,
            elements: props.subchapter.elements.filter((_, i) => i != pos),
          },
          props.subchapter.path,
        );
      },
      addSection: () => {
        subchapters.update(
          {
            ...props.subchapter,
            sections: [
              ...props.subchapter.sections,
              {
                type: segments.SegmentType.Section,
                title: 'Бызымянный пункт',
                elements: [],
                subsections: [],
              },
            ],
          },
          props.subchapter.path,
        );
      },
    });
  };

  createEffect(() => setupHandlers(), setupHandlers());

  return (
    <>
      <SegmentHeading
        title={props.subchapter.title}
        hierarchyTitle={segments.hierarchyTitle(props.subchapter)}
        onDelete={handlers.delete}
        onTitleUpdate={handlers.updateTitle}
      />
      <For each={props.subchapter.elements}>
        {(el, i) => (
          <SegmentElement
            element={el}
            onUpdate={handlers.updateElement(i())}
            onDelete={handlers.deleteElement(i())}
          />
        )}
      </For>
      <For each={props.subchapter.sections}>
        {(section, i) => (
          <ChildSegmentLink
            onclick={() =>
              selectSection({
                ...props.subchapter.path,
                section: i(),
              })
            }
            hierarchyTitle={segments.hierarchyTitle({
              ...section,
              path: { ...props.subchapter.path, section: i() },
            })}
          />
        )}
      </For>
      <AddContentButton onclick={() => setShowAddContentModal(true)} />
      <Show when={showAddContentModal()}>
        <AddSubchapterContentModal
          close={() => setShowAddContentModal(false)}
          onAddElement={(el) => handlers.addElement(el)}
          onAddSection={() => handlers.addSection()}
          showAddElement={props.subchapter.sections.length == 0}
          showAddSection={props.subchapter.elements.length == 0}
        />
      </Show>
    </>
  );
};

const SectionEditor: Component<{
  section: segments.SectionWithPath;
}> = (props) => {
  const [
    _r,
    { selectSubchapter, selectSection, selectSubsection },
  ] = useContext(SelectedSegmentContext);
  const [_s, { subchapters, sections }] = useContext(ReportContext);
  const [handlers, setHandlers] = createState<{
    delete: () => void;
    updateTitle: (title: string) => void;
    addElement: (el: elements.SegmentElement) => void;
    updateElement: (pos: number) => (el: elements.SegmentElement) => void;
    deleteElement: (pos: number) => () => void;
    addSubsection: () => void;
  }>(null);
  const [showAddContentModal, setShowAddContentModal] = createSignal(false);

  const setupHandlers = () => {
    setHandlers({
      delete: () => {
        const path = props.section.path; // need to copy it before select.
        const subchapter = subchapters.get(path);
        if (subchapter.sections.length == 1) {
          selectSubchapter(path);
        } else if (path.section == subchapter.sections.length - 1) {
          selectSection({ ...path, section: path.section - 1 });
        }
        sections.delete(path);
      },
      updateTitle: (title) => {
        sections.update({ ...props.section, title }, props.section.path);
      },
      addElement: (element: elements.SegmentElement) => {
        sections.update(
          {
            ...props.section,
            elements: [...props.section.elements, element],
          },
          props.section.path,
        );
      },
      updateElement: (pos: number) => (element: elements.SegmentElement) => {
        sections.update(
          {
            ...props.section,
            elements: props.section.elements.map((el, i) =>
              i == pos ? element : el,
            ),
          },
          props.section.path,
        );
      },
      deleteElement: (pos: number) => () => {
        sections.update(
          {
            ...props.section,
            elements: props.section.elements.filter((_, i) => i != pos),
          },
          props.section.path,
        );
      },
      addSubsection: () => {
        sections.update(
          {
            ...props.section,
            subsections: [
              ...props.section.subsections,
              {
                type: segments.SegmentType.Subsection,
                title: 'Бызымянный подпункт',
                elements: [],
              },
            ],
          },
          props.section.path,
        );
      },
    });
  };

  createEffect(() => setupHandlers(), setupHandlers());

  return (
    <>
      <SegmentHeading
        title={props.section.title}
        hierarchyTitle={segments.hierarchyTitle(props.section)}
        onDelete={handlers.delete}
        onTitleUpdate={handlers.updateTitle}
      />
      <For each={props.section.elements}>
        {(el, i) => (
          <SegmentElement
            element={el}
            onUpdate={handlers.updateElement(i())}
            onDelete={handlers.deleteElement(i())}
          />
        )}
      </For>
      <For each={props.section.subsections}>
        {(section, i) => (
          <ChildSegmentLink
            onclick={() =>
              selectSubsection({
                ...props.section.path,
                subsection: i(),
              })
            }
            hierarchyTitle={segments.hierarchyTitle({
              ...section,
              path: { ...props.section.path, subsection: i() },
            })}
          />
        )}
      </For>
      <AddContentButton onclick={() => setShowAddContentModal(true)} />
      <Show when={showAddContentModal()}>
        <AddSectionContentModal
          close={() => setShowAddContentModal(false)}
          onAddElement={(el) => handlers.addElement(el)}
          onAddSubsection={() => handlers.addSubsection()}
          showAddElement={props.section.subsections.length == 0}
          showAddSubsection={props.section.elements.length == 0}
        />
      </Show>
    </>
  );
};

const SubsectionEditor: Component<{
  subsection: segments.SubsectionWithPath;
}> = (props) => {
  const [_s, { selectSection, selectSubsection }] = useContext(
    SelectedSegmentContext,
  );
  const [_r, { sections, subsections }] = useContext(ReportContext);
  const [handlers, setHandlers] = createState<{
    delete: () => void;
    updateTitle: (title: string) => void;
    addElement: (el: elements.SegmentElement) => void;
    updateElement: (pos: number) => (el: elements.SegmentElement) => void;
    deleteElement: (pos: number) => () => void;
  }>(null);
  const [showAddContentModal, setShowAddContentModal] = createSignal(false);

  const setupHandlers = () => {
    setHandlers({
      delete: () => {
        const path = props.subsection.path; // need to copy it before select.
        const section = sections.get(path);
        if (section.subsections.length == 1) {
          selectSection(path);
        } else if (path.subsection == section.subsections.length - 1) {
          selectSubsection({ ...path, subsection: path.subsection - 1 });
        }
        subsections.delete(path);
      },
      updateTitle: (title) => {
        subsections.update(
          { ...props.subsection, title },
          props.subsection.path,
        );
      },
      addElement: (element: elements.SegmentElement) => {
        subsections.update(
          {
            ...props.subsection,
            elements: [...props.subsection.elements, element],
          },
          props.subsection.path,
        );
      },
      updateElement: (pos: number) => (element: elements.SegmentElement) => {
        subsections.update(
          {
            ...props.subsection,
            elements: props.subsection.elements.map((el, i) =>
              i == pos ? element : el,
            ),
          },
          props.subsection.path,
        );
      },
      deleteElement: (pos: number) => () => {
        subsections.update(
          {
            ...props.subsection,
            elements: props.subsection.elements.filter((_, i) => i != pos),
          },
          props.subsection.path,
        );
      },
    });
  };

  createEffect(() => setupHandlers(), setupHandlers());

  return (
    <>
      <SegmentHeading
        title={props.subsection.title}
        hierarchyTitle={segments.hierarchyTitle(props.subsection)}
        onDelete={handlers.delete}
        onTitleUpdate={handlers.updateTitle}
      />
      <For each={props.subsection.elements}>
        {(el, i) => (
          <SegmentElement
            element={el}
            onUpdate={handlers.updateElement(i())}
            onDelete={handlers.deleteElement(i())}
          />
        )}
      </For>
      <AddContentButton onclick={() => setShowAddContentModal(true)} />
      <Show when={showAddContentModal()}>
        <AddSubsectionContentModal
          close={() => setShowAddContentModal(false)}
          onAddElement={(el) => handlers.addElement(el)}
        />
      </Show>
    </>
  );
};

const SegmentHeading: Component<{
  title: string;
  hierarchyTitle: string;
  onDelete: () => void;
  onTitleUpdate: (title: string) => void;
}> = (props) => {
  const [showChangeTitleModal, setShowChangeTitleModal] = createSignal(false);
  const [showDeleteModal, setShowDeleteModal] = createSignal(false);

  return (
    <>
      <Show when={showChangeTitleModal()}>
        <ChangeTitleModal
          prevTitle={props.title}
          update={props.onTitleUpdate}
          close={() => setShowChangeTitleModal(false)}
        />
      </Show>
      <Show when={showDeleteModal()}>
        <ConfirmModal
          message="Вы действительно хотите удалить этот сегмент? Данные будут удалены безвозвратно."
          confirm={() => {
            setShowDeleteModal(false);
            props.onDelete();
          }}
          close={() => setShowDeleteModal(false)}
        />
      </Show>
      <Card class="flex justify-between items-center">
        <h1 class="text-lg font-bold leading-tight overflow-ellipsis overflow-hidden">
          {props.hierarchyTitle}
        </h1>
        <div class="ml-4 flex-shrink-0 flex space-x-2">
          <button onclick={() => setShowChangeTitleModal(true)}>
            <PenIcon class="w-5 h-5" />
          </button>
          <button onclick={() => setShowDeleteModal(true)}>
            <TrashIcon class="w-5 h-5" />
          </button>
        </div>
      </Card>
    </>
  );
};

const ChildSegmentLink: Component<{
  hierarchyTitle: string;
  onclick: () => void;
}> = (props) => {
  return (
    <Card class="mt-6 p-0">
      <Button
        onclick={props.onclick}
        class="w-full flex justify-between items-center bg-transparent focus:ring-0 focus:bg-gray-100 hover:ring-0 hover:bg-gray-100"
      >
        <h5 class="text-left leading-tight overflow-ellipsis overflow-hidden">
          {props.hierarchyTitle}
        </h5>
        <div class="ml-4 flex-shrink-0">
          <ArrowRight />
        </div>
      </Button>
    </Card>
  );
};

const AddContentButton: Component<{
  onclick: () => void;
}> = (props) => {
  return (
    <div class="mt-6 flex justify-center">
      <Card class="p-0">
        <Button
          onclick={props.onclick}
          class="p-0 bg-white flex justif-center items-center focus:ring-0 focus:bg-gray-100 hover:ring-0 hover:bg-gray-100"
        >
          <PlusIcon class="w-8 h-8" />
        </Button>
      </Card>
    </div>
  );
};
