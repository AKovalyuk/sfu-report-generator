import { Card } from '@/components';
import { ReportContext } from '@/contexts/report';
import { Component, useContext } from 'solid-js';

export const Preview: Component = () => {
  const [report] = useContext(ReportContext);

  return (
    <Card class="h-full max-h-full overflow-auto">
      <pre>{JSON.stringify(report, null, 2)}</pre>
    </Card>
  );
};
