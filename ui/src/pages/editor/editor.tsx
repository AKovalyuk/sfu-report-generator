import { Component, onMount, useContext } from 'solid-js';
import { Minimap } from './minimap';
import { ReportContext } from '@/contexts/report';
import { SelectedSegmentContext } from '@/contexts/selected';
import { SegmentEditor } from './segment';
import { Preview } from '@/pages/editor/preview';

export const EditorPage: Component = () => {
  const [report] = useContext(ReportContext);
  const [_, { selectChapter }] = useContext(SelectedSegmentContext);

  onMount(() => {
    if (report.chapters.length > 0) {
      selectChapter({ chapter: 0 });
    }
  });

  return (
    <div class="h-full grid grid-cols-[3fr,4fr,5fr] gap-6">
      <div class="min-w-0">
        <Minimap />
      </div>
      <div class="min-w-0">
        <SegmentEditor />
      </div>
      <div class="min-w-0 min-h-0 max-h-full">
        <Preview />
      </div>
    </div>
  );
};
