FROM node:14

RUN npm install -g pnpm

WORKDIR /app/ui

COPY ui/package.json .
COPY ui/pnpm-lock.yaml .

RUN pnpm install

COPY ui .

EXPOSE 8080
CMD ["pnpm", "dev"]

